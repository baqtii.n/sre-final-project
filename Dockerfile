# Use Node.js 12.21.0 Buster Slim as the base image
FROM node:12.21.0-buster-slim as base

# This image is NOT made for production use.
LABEL maintainer="Eero Ruohola <eero.ruohola@shuup.com>"

# Update the package list and install necessary packages
RUN apt-get update && \
    apt-get --assume-yes install \
        libpangocairo-1.0-0 \
        python3 \
        python3-dev \
        python3-pil \
        python3-pip \
        libffi-dev \
        curl && \
    rm -rf /var/lib/apt/lists/ /var/cache/apt/

# Install Rust
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
ENV PATH="/root/.cargo/bin:${PATH}"

# Copy the current directory contents into the container at /app
COPY . /app
WORKDIR /app

# Upgrade pip, set the environment variable, and downgrade markupsafe
RUN pip3 install --upgrade pip && \
    export CRYPTOGRAPHY_DONT_BUILD_RUST=1 && \
    pip3 install markupsafe==1.1.1

# The dev compose file sets this to 1 to support development and editing the source code.
# The default value of 0 just installs the demo for running.
ARG editable=0

# Install Python dependencies
RUN if [ "$editable" -eq 1 ]; then pip3 install -r requirements-tests.txt && python3 setup.py build_resources; else pip3 install shuup django-prometheus; fi

# Run migrations and initialize Shuup
RUN python3 -m shuup_workbench migrate && \
    python3 -m shuup_workbench shuup_init

# Create a superuser
RUN echo '\
from django.contrib.auth import get_user_model\n\
from django.db import IntegrityError\n\
try:\n\
    get_user_model().objects.create_superuser("admin", "admin@admin.com", "admin")\n\
except IntegrityError:\n\
    pass\n'\
| python3 -m shuup_workbench shell

# Start the server
CMD ["python3", "-m", "shuup_workbench", "runserver", "0.0.0.0:8000"]
